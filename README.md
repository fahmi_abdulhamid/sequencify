# Sequencify

An Aviarc application to quickly draw sequence diagrams. Graphite was used to create the front-end and custom widgets were developed in SourceEditor.


## Libraries used

* [js-sequence-diagrams](https://bramp.github.io/js-sequence-diagrams/): Renders sequence diagrams as SVG.
* [canvg](https://github.com/canvg/canvg): Renders SVG to Canvas (for image export).