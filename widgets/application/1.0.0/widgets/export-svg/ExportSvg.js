/*global
YAHOO
*/

(function() {
    YAHOO.namespace("application.v1_0_0");
    var application = YAHOO.application.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.ExportSvg = function() {

    };

    YAHOO.lang.extend(application.ExportSvg, Toronto.framework.DefaultActionImpl, {
        run: function(state) {
            var targetWidgetName = this.getAttribute('target-widget', state);

            if (!targetWidgetName) {
                return;
            }

            var targetWidget = Toronto.findWidget(targetWidgetName);

            if (!targetWidget) {
                throw new Error('Cannot find widget with name: ' + targetWidgetName);
            }

            var targetContainerElement = targetWidget.getContainerElement();
            if (!targetContainerElement) {
                throw new Error('Widget does not have a container element: ' + targetWidgetName);
            }

            var svgElement = targetContainerElement.getElementsByTagName('svg')[0];
            if (!svgElement) {
                throw new Error('Widget does not have an inner SVG element: ' + targetWidgetName);
            }
            
            var svgElementBounds = svgElement.getBoundingClientRect();
            var width = Math.round(svgElementBounds.width);
            var height = Math.round(svgElementBounds.height);

            var canvas = document.createElement('CANVAS');
            canvas.style.width = width;
            canvas.style.height = height;
            
            canvg(canvas, svgElement.outerHTML);
            
            canvas.toBlob(function (blob) {
                var fileName = this.getAttribute('file-name', state) + '.png';
                this._download(fileName, blob);
            }.bind(this), 'image/png');
        },

        _download(filename, blob) {
            var element = document.createElement('a');
            element.setAttribute('href', window.URL.createObjectURL(blob));
            element.setAttribute('download', filename);

            element.style.display = 'none';
            document.body.appendChild(element);

            element.click();

            document.body.removeChild(element);
        }
    });
})();