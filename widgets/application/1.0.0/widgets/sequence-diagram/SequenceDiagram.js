/*global
YAHOO
*/

(function () {
    YAHOO.namespace("application.v1_0_0");
    var application = YAHOO.application.v1_0_0;
    var Toronto = YAHOO.com.aviarc.framework.toronto;

    application.SequenceDiagram = function() {
        application.SequenceDiagram.superclass.constructor.apply(this, arguments);
        
        this._fieldCfg = null;
        this._dataset = null;
        this._refreshEvent = null;
        this._rowChangeEvent = null;
    };

    YAHOO.lang.extend(application.SequenceDiagram, Toronto.framework.DefaultWidgetImpl, {
        startup: function (widgetContext) {
            application.SequenceDiagram.superclass.startup.apply(this, arguments);
        },
        
        bind: function(dataContext) {
            application.SequenceDiagram.superclass.bind.apply(this, arguments);
            
            if(this._refreshEvent) {
                this._refreshEvent.unbind();
                this._rowChangeEvent.unbind();
            }
            
            this._fieldCfg = this._getFieldCfg();
            if(!this._fieldCfg) {
                return;
            }
            
            this._dataset = dataContext.findDataset(this._fieldCfg.datasetName);
            this._refreshEvent = this._dataset.bindOnCurrentRowFieldChangedHandler(this._fieldCfg.fieldName, this.refresh, this);
            this._rowChangeEvent = this._dataset.onCurrentRowChanged.bindHandler(this.refresh, this);
        },
        
        refresh: function() {
            application.SequenceDiagram.superclass.refresh.apply(this, arguments);
            
            var markup = this._dataset.getCurrentRowField(this._fieldCfg.fieldName);
            
            var drawingElement = this.getContainerElement();
            drawingElement.innerHTML = '';
            
            try {
                var diagram = Diagram.parse(markup);
                diagram.drawSVG(drawingElement, {theme: 'simple'});
            } catch (parseError) {
                var pre = document.createElement('pre');
                pre.textContent = parseError.message;
                drawingElement.appendChild(pre);
            }
        },
        
        _getFieldCfg: function() {
            var fieldPath = this.getAttribute('field');
            if(!fieldPath) {
                return null;
            }
            
            var fieldParts = fieldPath.split('.');
            
            return  {
                datasetName: fieldParts[0],
                fieldName: fieldParts[1]
            };
        }
    });
})();
